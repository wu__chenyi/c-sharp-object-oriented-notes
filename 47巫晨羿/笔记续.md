## 10类的静态成员

1. 静态方法是用类来调用的

2. 静态方法只能访问到静态成员变量，不要用this

3. 使⽤static修饰的类为静态类，静态类所有成员都必须是静态的，不能与abstract、sealed ⼀起使⽤。

   静态类不能被实例化。

4. 访问器：  get 是读，set 是写

## 11结构体

```c#
访问修饰符 struct 结构体命
{
	定义结构体成员；

}
```

例如：

```c#
struct Books
{
   public string title;
   public string author;
   public string subject;
   public int book_id;
};  
```



## 12索引器

索引器是一种特殊的类成员

允许按照数组的方式检索对象的数组元素

索引器定义语法与属性定义语法类似

```c#
[访问修饰符] 数据类型 this [数据类型 index]
{
	get{return this.变量名;}
	set{this.变量名[index]=value;}

}
```



## 13委托

```
委托（Delegate）是存有对某个方法的引用的一种引用类型变量。引用可在运行时被改变。

委托（Delegate）特别用于实现事件和回调方法。所有的委托（Delegate）都派生自 System.Delegate 类。
```

声明委托

```c#
声明委托（Delegate）
委托声明决定了可由该委托引用的方法。委托可指向一个与其具有相同标签的方法。

例如，假设有一个委托：

public delegate int MyDelegate (string s);
上面的委托可被用于引用任何一个带有一个单一的 string 参数的方法，并返回一个 int 类型变量。

声明委托的语法如下：

delegate <return type> <delegate-name> <parameter list>
```

实例化委托

```c#
实例化委托（Delegate）
一旦声明了委托类型，委托对象必须使用 new 关键字来创建，且与一个特定的方法有关。当创建委托时，传递到 new 语句的参数就像方法调用一样书写，但是不带有参数。例如：

public delegate void printString(string s);
//
printString ps1 = new printString(WriteToScreen);
printString ps2 = new printString(WriteToFile);
```

例如：

```c#
using System;

delegate int NumberChanger(int n);
namespace DelegateAppl
{
   class TestDelegate
   {
      static int num = 10;
      public static int AddNum(int p)
      {
         num += p;
         return num;
      }

      public static int MultNum(int q)
      {
         num *= q;
         return num;
      }
      public static int getNum()
      {
         return num;
      }

      static void Main(string[] args)
      {
         // 创建委托实例
         NumberChanger nc1 = new NumberChanger(AddNum);
         NumberChanger nc2 = new NumberChanger(MultNum);
         // 使用委托对象调用方法
         nc1(25);
         Console.WriteLine("Value of Num: {0}", getNum());
         nc2(5);
         Console.WriteLine("Value of Num: {0}", getNum());
         Console.ReadKey();
      }
   }
}
```

## 14匿名方法

1. 匿名方法是通过使用 delegate 关键字创建委托实例来声明的。例如：

```c#
delegate void NumberChanger(int n);
//
NumberChanger nc = delegate(int x)
{
    Console.WriteLine("Anonymous Method: {0}", x);
};

代码块 Console.WriteLine("Anonymous Method: {0}", x);是匿名方法的主体。

委托可以通过匿名方法调用，也可以通过命名方法调用，即，通过向委托对象传递方法参数。
```

2. 实例

```c#
using System;

delegate void NumberChanger(int n);
namespace DelegateAppl
{
    class TestDelegate
    {
        static int num = 10;
        public static void AddNum(int p)
        {
            num += p;
            Console.WriteLine("Named Method: {0}", num);
        }

        public static void MultNum(int q)
        {
            num *= q;
            Console.WriteLine("Named Method: {0}", num);
        }

        static void Main(string[] args)
        {
            // 使用匿名方法创建委托实例
            NumberChanger nc = delegate(int x)
            {
               Console.WriteLine("Anonymous Method: {0}", x);
            };
           
            // 使用匿名方法调用委托
            nc(10);

            // 使用命名方法实例化委托
            nc =  new NumberChanger(AddNum);
           
            // 使用命名方法调用委托
            nc(5);

            // 使用另一个命名方法实例化委托
            nc =  new NumberChanger(MultNum);
           
            // 使用命名方法调用委托
            nc(2);
            Console.ReadKey();
        }
    }
}
```

