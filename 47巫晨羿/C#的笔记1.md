# 笔记

## 1

1. 输出

   ```c#
   Console.Write();  //不换行
   Console.WriteLine("Hello World!");  //换行
   
   ```

   

2. 输入

   ```c#
   string a =  Console.ReadLine();//默认输入的数据类型为string
   Console.WriteLine(a);
   
   ```

3. 变量

   ```c#
   //规则
   //组成：52个字母(A-Z, a-z)，10个数字(0-9)，下划线(_)
   //开头：字母或下划线
   //不能是关键字
   
   
   int b = 6;
   string c = "abc";
   var d = 5;
   ar e = "abc";
   int(32位) short(16位)  byte
   //有符号，无符号 
   //有符号 ： short（-32,768到32,767之间的有符号整数值）  
   //无符号 ： ushort（0到65,535之间的无符号整数值）
   ```

   

4. 常量

   ```c#
   const int e1 = 7;
   ```

5. 数据类型

   ```c#
   //整型：byte,short,int,long,
   //浮点型：float,double,char
   //布尔型: bool
   //小数 : decimal(4,2) 4表示长度 ， 2表示小数点后位数
   decimal a1 = 4.0m;//如果没有后缀 m，则数字将被视为 double 类型并会生成编译器错误。
   
   ```

6. 运算符 : 算术，比较，逻辑，三元

   ```c#
    //算数：+ - * / %
    //赋值运算：+=,-=,*=,/=,%=
    //b += 1;// b= b+1, b++,++b
    //比较运算符：>,<,>=,<=,!=,==
    //bool b1 = b >= 7;
    //逻辑：&&, ||, !
    //三元： (条件表达式A)?A为true:A为false
      int a = 5;
      int b = 8;
      int c = 7;
      int max = a > b ? (a>c?a:c) : (b>c?b:c);
      Console.WriteLine(max);
   ```

7. 定义类型

   ```c#
   //输入一个int 数据类型
   int a1 = Convert.ToInt32(Console.ReadLine());
   ```

   

8. 选择结构

   ```c#
   if（条件表达式1）
   {
       条件表达式1 为true，则执行
   }else if(条件表达式2)
   {
        条件表达式2 为true，则执行  
   }else
   {
       上面都不执行则执行
   }
   ```

   

9. 循环结构          四要素:1.初始值 2.判断条件 3.循环体 4.迭代因子

   1. while 循环

   ```c#
   //求 1+...+10 的和
     int i = 1;	//初始值
     while (i<=10)//判断条件
     {
     sum = sum + i;//循环体
     i++;			//迭代因子
     }
     Console.WriteLine(sum);//输出
   ```

   2. for 循环

      ```c#
      for(初始值;判断条件;迭代因子)
      {
          循环体
      }
      ```

   3. foreach循环

      ```c#
      //用于列举出集合中所有的元素，foreach 语句中的表达式由关键字 in 隔开的两个项组成。
      //in 左边的项是变量名，in 右边的项是集合名，用来存放该集合中的每个元素。
      foreach(数据类型  变量名  in  数组名)
      {
          //语句块；
      }
      ```

      示例

      ```c#
       int countChar = 0, countNumber = 0, countSym = 0;
                  Console.WriteLine("请输入一串字符：");
                  string str = Console.ReadLine();  //fakghiag  4#&$#E
                  foreach (char j in str)
                  {
                      if (j >= 'A' && j <= 'Z' || j >= 'a' && j <= 'z')
                          //0 : 30;     A: 41 ;     a: 61 (参照ASCII 表)
                      {
                          countChar++;
                      }
                      else if (j >= '0' && j <= '9')
                      {
                          countNumber++;
                      }
                      else
                      {
                          countSym++;
                      }
                  }
      
                     Console.WriteLine("数字{0},\t字母{1},\t特殊符号{2}\n", countNumber, countChar, countSym);
      			//Console.WriteLine($ "数字{countNumber},\t字母{countChar},\t特殊符号{countChar}\n");
      			//转义字符必须在""中使用
      ```

10. 转义字符

    | \n   | 换行                                           |
    | ---- | ---------------------------------------------- |
    | \r   | 回车                                           |
    | \t   | 制表符                                         |
    | \f   | 换页符                                         |
    | \b   | 退格                                           |
    | \a   | 响铃                                           |
    | \e   | escape（ASCII中的escape 字符）                 |
    | \007 | 任何八进制值（这里是，007=bell(响铃)）         |
    | \x7f | 任何十六进制值（这里是，007=bell）             |
    | \cC  | 一个控制符（这里是：Ctrl+c）                   |
    | \l   | 下个字符小写                                   |
    | \L   | 接着的字符均小写直到\E                         |
    | \u   | 下个字符大写                                   |
    | \U   | 接着的字符均大写直到\E                         |
    | \Q   | 在 non-word 字符前加上(自动加转义符号)，直到\E |
    | \E   | 结束\L,\E和\Q                                  |
    | \0   | 空格                                           |
    |      |                                                |

## 2命名规范

```c#
//小驼峰 ： 首个单词的首字母小写，其余单词的首字母大写
//常用于变量，方法
int myAge = 5; static void myClassInfo(); 

//大驼峰 ： 所有单词首字母大写
//用于类名，属性，命名空间
public class DataBaseUser 
```

## 3类型转换

```c#
1. 
string myString = "Hello";
int myInt = int.Parse(myString);
2. 
double score = 92.6;
string myString = score.ToString( );
3. 
double score = 59.3;
int myInt = Convert.ToInt32 (score);

```



## 4数组

### 1数组



```c#
  int[] bub = { 8, 7, 4, 6, 5, 3 };//静态
            //外层for控制第几趟
            for(int i = 0; i < bub.Length; i++)
            {
                //每一趟找出最大往下沉
                for(int j = 0; j < bub.Length-1-i; j++)
                {
                    if (bub[j] > bub[j + 1])
                    {
                        int temp = bub[j];
                        bub[j] = bub[j + 1];
                        bub[j + 1] = temp;
                    }
                }
            }
            foreach (var i in bub)
            {
                Console.WriteLine(i);
            }

```

### 2矩阵数组

  二维数组 同 多维数组 又称矩形数组

```c#
1. 
int[,] myArray;
            myArray = new int[2, 3];//2行 3列
            myArray[0, 0] = 1;
            myArray[0, 1] = 2;
            myArray[0, 2] = 3;


            myArray[1, 0] = 4;
            myArray[1, 1] = 5;
            myArray[1, 2] = 6;


            foreach (var item in myArray)
            {
                Console.WriteLine(item);
            }

2. 
int[,] arr = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

            //for
            for (int i = 0; i < arr.Getlength(0); i++)
            {
                for (int j = 0; j < arr.Getlength(1); j++)
                
                    Console.Writeline(arr[i, j]);
                }
            }

            //foreach
            foreach (var i in arr)
            {
                Console.Writeline(i);
            }
```



## 5随机数



```c#
Random a = new Random();//产生随机数
Console.WriteLine(a.Next(10)); //不包含10
```

## 6枚举

```c#
 enum student
         {   
            周飘,
            金星=10,
            曾鹏
         };
//放在main 方法外
```







