# 笔记

## 1

1. 输出

   ```c#
   Console.Write();  //不换行
   Console.WriteLine("Hello World!");  //换行
   
   ```

   

2. 输入

   ```c#
   string a =  Console.ReadLine();//默认输入的数据类型为string
   Console.WriteLine(a);
   
   ```

3. 变量

   ```c#
   //规则
   //组成：52个字母(A-Z, a-z)，10个数字(0-9)，下划线(_)
   //开头：字母或下划线
   //不能是关键字
   
   
   int b = 6;
   string c = "abc";
   var d = 5;
   ar e = "abc";
   int(32位) short(16位)  byte
   //有符号，无符号 
   //有符号 ： short（-32,768到32,767之间的有符号整数值）  
   //无符号 ： ushort（0到65,535之间的无符号整数值）
   ```

   

4. 常量

   ```c#
   const int e1 = 7;
   ```

5. 数据类型

   ```c#
   //整型：byte,short,int,long,
   //浮点型：float,double,char
   //布尔型: bool
   //小数 : decimal(4,2) 4表示长度 ， 2表示小数点后位数
   decimal a1 = 4.0m;//如果没有后缀 m，则数字将被视为 double 类型并会生成编译器错误。
   
   ```

6. 运算符 : 算术，比较，逻辑，三元

   ```c#
    //算数：+ - * / %
    //赋值运算：+=,-=,*=,/=,%=
    //b += 1;// b= b+1, b++,++b
    //比较运算符：>,<,>=,<=,!=,==
    //bool b1 = b >= 7;
    //逻辑：&&, ||, !
    //三元： (条件表达式A)?A为true:A为false
      int a = 5;
      int b = 8;
      int c = 7;
      int max = a > b ? (a>c?a:c) : (b>c?b:c);
      Console.WriteLine(max);
   ```

7. 定义类型

   ```c#
   //输入一个int 数据类型
   int a1 = Convert.ToInt32(Console.ReadLine());
   ```

   

8. 选择结构

   ```c#
   if（条件表达式1）
   {
       条件表达式1 为true，则执行
   }else if(条件表达式2)
   {
        条件表达式2 为true，则执行  
   }else
   {
       上面都不执行则执行
   }
   ```

   

9. 循环结构          四要素:1.初始值 2.判断条件 3.循环体 4.迭代因子

   1. while 循环

   ```c#
   //求 1+...+10 的和
     int i = 1;	//初始值
     while (i<=10)//判断条件
     {
     sum = sum + i;//循环体
     i++;			//迭代因子
     }
     Console.WriteLine(sum);//输出
   ```

   2. for 循环

      ```c#
      for(初始值;判断条件;迭代因子)
      {
          循环体
      }
      ```

   3. foreach循环

      ```c#
      //用于列举出集合中所有的元素，foreach 语句中的表达式由关键字 in 隔开的两个项组成。
      //in 左边的项是变量名，in 右边的项是集合名，用来存放该集合中的每个元素。
      foreach(数据类型  变量名  in  数组名)
      {
          //语句块；
      }
      ```

      示例

      ```c#
       int countChar = 0, countNumber = 0, countSym = 0;
                  Console.WriteLine("请输入一串字符：");
                  string str = Console.ReadLine();  //fakghiag  4#&$#E
                  foreach (char j in str)
                  {
                      if (j >= 'A' && j <= 'Z' || j >= 'a' && j <= 'z')
                          //0 : 30;     A: 41 ;     a: 61 (参照ASCII 表)
                      {
                          countChar++;
                      }
                      else if (j >= '0' && j <= '9')
                      {
                          countNumber++;
                      }
                      else
                      {
                          countSym++;
                      }
                  }
      
                     Console.WriteLine("数字{0},\t字母{1},\t特殊符号{2}\n", countNumber, countChar, countSym);
      			//Console.WriteLine($ "数字{countNumber},\t字母{countChar},\t特殊符号{countChar}\n");
      			//转义字符必须在""中使用
      ```

10. 转义字符

    | \n   | 换行                                           |
    | ---- | ---------------------------------------------- |
    | \r   | 回车                                           |
    | \t   | 制表符                                         |
    | \f   | 换页符                                         |
    | \b   | 退格                                           |
    | \a   | 响铃                                           |
    | \e   | escape（ASCII中的escape 字符）                 |
    | \007 | 任何八进制值（这里是，007=bell(响铃)）         |
    | \x7f | 任何十六进制值（这里是，007=bell）             |
    | \cC  | 一个控制符（这里是：Ctrl+c）                   |
    | \l   | 下个字符小写                                   |
    | \L   | 接着的字符均小写直到\E                         |
    | \u   | 下个字符大写                                   |
    | \U   | 接着的字符均大写直到\E                         |
    | \Q   | 在 non-word 字符前加上(自动加转义符号)，直到\E |
    | \E   | 结束\L,\E和\Q                                  |
    | \0   | 空格                                           |
    |      |                                                |

## 2命名规范

```c#
//小驼峰 ： 首个单词的首字母小写，其余单词的首字母大写
//常用于变量，方法
int myAge = 5; static void myClassInfo(); 

//大驼峰 ： 所有单词首字母大写
//用于类名，属性，命名空间
public class DataBaseUser 
```

## 3类型转换

```c#
1. 
string myString = "Hello";
int myInt = int.Parse(myString);
2. 
double score = 92.6;
string myString = score.ToString( );
3. 
double score = 59.3;
int myInt = Convert.ToInt32 (score);

```



## 4数组

### 1数组



```c#
  int[] bub = { 8, 7, 4, 6, 5, 3 };//静态
            //外层for控制第几趟
            for(int i = 0; i < bub.Length; i++)
            {
                //每一趟找出最大往下沉
                for(int j = 0; j < bub.Length-1-i; j++)
                {
                    if (bub[j] > bub[j + 1])
                    {
                        int temp = bub[j];
                        bub[j] = bub[j + 1];
                        bub[j + 1] = temp;
                    }
                }
            }
            foreach (var i in bub)
            {
                Console.WriteLine(i);
            }

```

### 2矩阵数组

  二维数组 同 多维数组 又称矩形数组

```c#
1. 
int[,] myArray;
            myArray = new int[2, 3];//2行 3列
            myArray[0, 0] = 1;
            myArray[0, 1] = 2;
            myArray[0, 2] = 3;


            myArray[1, 0] = 4;
            myArray[1, 1] = 5;
            myArray[1, 2] = 6;


            foreach (var item in myArray)
            {
                Console.WriteLine(item);
            }

2. 
int[,] arr = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

            //for
            for (int i = 0; i < arr.Getlength(0); i++)
            {
                for (int j = 0; j < arr.Getlength(1); j++)
                
                    Console.Writeline(arr[i, j]);
                }
            }

            //foreach
            foreach (var i in arr)
            {
                Console.Writeline(i);
            }
```



## 5随机数



```c#
Random a = new Random();//产生随机数
Console.WriteLine(a.Next(10)); //不包含10
```

## 6枚举

```c#
 enum student
         {   
            周飘,
            金星=10,
            曾鹏
         };
//放在main 方法外
```



## 7方法

### 1修饰符



访问修饰符：public（公共的）,private(私有的),protected(受保护

的),internal(内部的)

### 2调用非静态

```c#
class Program
    {
        public static void Main(string[] args)
        {
            Program p = new Program();
            p.hi();
        }
        public extern void hi()
        {
            Console.Write("Hello C#");
        }
    }
```



### 3传递

Java只有值传递，C#除此之外还有ref（引用）传递，out（输出）传递

值传递只能返回一个，ref，out可以返回多个值

value：实参和形参互相独立, 只进不出

ref:能改变实际参数的值,   又进又出

out ：  只出不进

```c#
using System;

//value(值传递)、ref(引用传递)、out(输出传递)
public class Test
{
    public static void ValueFun(int i)
    {
        i++;
    }

    public static void RefFun(ref int j)
    {
        j++;
    }

    public static void OutFun(out int k)
    {
        k = 0;//使用out关键字，必须参数初始化
        k++;
    }

    static void Main()
    {
        int i = 0;
        ValueFun(i);//i的值是实参的一个副本，实参i不会改变
        Console.WriteLine(i);

        int j = 0;
        RefFun(ref j);//指向同一块内存，实参j的值会改变
        Console.WriteLine(j);

        int k;
        OutFun(out k);//和ref关键字等效
        Console.WriteLine(k);
    }
}

```



### 4重载和重写

1. 区别

   重载(overload)和重写(override)有什么区别
   重载:方法名一样，但是：1.形参列表个数不一致；2.形参列表类型不一致；3.形参列表顺序不一致。

```c#
public static void sum(int a, int b){} //1
public static void sum(int a, int b, int c){} //2
public static void sum(int a, double b){} //3
public static void sum(double a,int b){}//4
//1.形参个数不同: 1和2
//2.相同个数，形参类型不同：1，3，4
//3.形参数据类型顺序不一致
```

### 5.递归

​	方法自己调用自己，递归2要素：①递归出口 ②递归表达式



## 8面向对象

### 1

​	面向对象(三大特性)：封装，继承，多态， 
​	Java,C#完全面向对象，C++（泛型）
​	面向过程：C语言，C++
​	戴口罩--》出门--》下楼梯--》走路--》点菜--》付款--》脱口罩--》吃饭

​	面向对象：我要吃饭--》吃饭    

​	类，万物皆对象
​	类：分类：人类（男人，女人，学生，歌手，演员）

​	对象：事物的具体

### 2类与对象的使用

1. 类的语法：

```c#
[访问修饰符] class 类名{

           字段;//描述对象的状态(声明变量)

           方法;//实现对象的行为

           构造函数;//初始化对象

      }
```

2. 实例化类

3. 产生对象

4. 使用对象

### 3成员方法和构造方法

1. 成员方法

​	概念：类里面定义的方法

​	语法结构：

```c#
[访问修饰符] 返回类型 <方法名> (参数列表)

{

 //方法体

}
```

​	成员变量

​	方法调用

2. 构造方法

   是类中的一种特殊的方法，构造函数名与类名相同，不返回任何值，可初始化成员变量

   语法结构

   ```c#
   class Role
       {
           // 1、创建一个控制台应用程序，该程序名为ClassTest,
           //在该程序中创建一个以Role为类名的、包含以下成员的一个类。
           //角色名、性别、出处、地位、门派地位、爱侣、父亲、母亲：
           public string roleName;
           public string gender;
           public string provenance;
           public string status;
           public string fstatus;
           public string lovepartner;
           public string father;
           public string mother;
   
           public Role(string RoleName, string Gender, string Provenance,
               string Status, string Fstatus, string Lovepartner, string Father, string Mother)
           {
               this.roleName = RoleName;
               this.gender = Gender;
               this.provenance = Provenance;
               this.status = Status;
               this.fstatus = Fstatus;
               this.lovepartner = Lovepartner;
               this.father = Father;
               this.mother = Mother;
           }
           public void plase()
           {
               Console.WriteLine("{0},{1}的，来自{2}，很{3}，是{4}，爱侣是{5}，父亲是{5}，母亲是{6}", roleName, gender, provenance, status, fstatus, lovepartner, father,mother) ;
           }
   
       }
   ```

   ```c#
   Role H = new Role("林黛玉", "男", "北京", "高大上", "第一", "贾宝玉", "林某某", "贾某某");
               H.plase();
               Console.ReadLine();
   ```

   

3. 如果没有为类编写构造方法，系统自动生成无参数的构造方法，称为模式构造方法

   a.无参构造方法

   b.有参构造方法

   构造方法可以更简捷的为对象赋初值,为对象开辟内存空间

   例化对象时才能调用,This关键字,当前操作的对象

   对象在创建的过程中有一个过程，当new的时候，会调用构造方法，在构造方法中可以做对象初始化操作

   构造方法与成员方法不同，它是在对象创建时自动调用

   构造方法可以有参数，也可以无参数

   构造方法无返回值

   构造方法名与类名相同

   如果定义的类没有定义构造方法，对象在创建时默认使用空构造

## 9装箱和拆箱

//存与堆内存
栈区:存放值类型的对象本身，引用类型的引用地址（指针），
静态区对象的引用地址（指针），常量区对象的引用地址（指针）等
堆区:用于存放引用类型对象本身

```c#
//装箱和拆箱
//装箱:将值类型转换为引用类型
int value =9;
Object obj =value;
//拆箱:将引用类型转换为值类型
int values=10;
Object obj =value;
int b=(int)Obj;
```

## 10类的静态成员

1. 静态方法是用类来调用的

2. 静态方法只能访问到静态成员变量，不要用this

3. 使⽤static修饰的类为静态类，静态类所有成员都必须是静态的，不能与abstract、sealed ⼀起使⽤。

   静态类不能被实例化。

4. 访问器：  get 是读，set 是写

## 11结构体

```c#
访问修饰符 struct 结构体命
{
	定义结构体成员；

}
```

例如：

```c#
struct Books
{
   public string title;
   public string author;
   public string subject;
   public int book_id;
};  
```



## 12索引器

索引器是一种特殊的类成员

允许按照数组的方式检索对象的数组元素

索引器定义语法与属性定义语法类似

```c#
[访问修饰符] 数据类型 this [数据类型 index]
{
	get{return this.变量名;}
	set{this.变量名[index]=value;}

}
```



## 13委托

```
委托（Delegate）是存有对某个方法的引用的一种引用类型变量。引用可在运行时被改变。

委托（Delegate）特别用于实现事件和回调方法。所有的委托（Delegate）都派生自 System.Delegate 类。
```

声明委托

```c#
声明委托（Delegate）
委托声明决定了可由该委托引用的方法。委托可指向一个与其具有相同标签的方法。

例如，假设有一个委托：

public delegate int MyDelegate (string s);
上面的委托可被用于引用任何一个带有一个单一的 string 参数的方法，并返回一个 int 类型变量。

声明委托的语法如下：

delegate <return type> <delegate-name> <parameter list>
```

实例化委托

```c#
实例化委托（Delegate）
一旦声明了委托类型，委托对象必须使用 new 关键字来创建，且与一个特定的方法有关。当创建委托时，传递到 new 语句的参数就像方法调用一样书写，但是不带有参数。例如：

public delegate void printString(string s);
//
printString ps1 = new printString(WriteToScreen);
printString ps2 = new printString(WriteToFile);
```

例如：

```c#
using System;

delegate int NumberChanger(int n);
namespace DelegateAppl
{
   class TestDelegate
   {
      static int num = 10;
      public static int AddNum(int p)
      {
         num += p;
         return num;
      }

      public static int MultNum(int q)
      {
         num *= q;
         return num;
      }
      public static int getNum()
      {
         return num;
      }

      static void Main(string[] args)
      {
         // 创建委托实例
         NumberChanger nc1 = new NumberChanger(AddNum);
         NumberChanger nc2 = new NumberChanger(MultNum);
         // 使用委托对象调用方法
         nc1(25);
         Console.WriteLine("Value of Num: {0}", getNum());
         nc2(5);
         Console.WriteLine("Value of Num: {0}", getNum());
         Console.ReadKey();
      }
   }
}
```

## 14匿名方法

1. 匿名方法是通过使用 delegate 关键字创建委托实例来声明的。例如：

```c#
delegate void NumberChanger(int n);
//
NumberChanger nc = delegate(int x)
{
    Console.WriteLine("Anonymous Method: {0}", x);
};

代码块 Console.WriteLine("Anonymous Method: {0}", x);是匿名方法的主体。

委托可以通过匿名方法调用，也可以通过命名方法调用，即，通过向委托对象传递方法参数。
```

2. 实例

```c#
using System;

delegate void NumberChanger(int n);
namespace DelegateAppl
{
    class TestDelegate
    {
        static int num = 10;
        public static void AddNum(int p)
        {
            num += p;
            Console.WriteLine("Named Method: {0}", num);
        }

        public static void MultNum(int q)
        {
            num *= q;
            Console.WriteLine("Named Method: {0}", num);
        }

        static void Main(string[] args)
        {
            // 使用匿名方法创建委托实例
            NumberChanger nc = delegate(int x)
            {
               Console.WriteLine("Anonymous Method: {0}", x);
            };
           
            // 使用匿名方法调用委托
            nc(10);

            // 使用命名方法实例化委托
            nc =  new NumberChanger(AddNum);
           
            // 使用命名方法调用委托
            nc(5);

            // 使用另一个命名方法实例化委托
            nc =  new NumberChanger(MultNum);
           
            // 使用命名方法调用委托
            nc(2);
            Console.ReadKey();
        }
    }
}
```



## 15 Lambda表达式

例子：

```c#
delegate int myDel(int a,int b);
    delegate int myDel1(int a);
    class Program
    {
        static void Main(string[] args)
        {
			//如果委托有多个参数，左侧参数必须被“（）”包裹
            myDel md =  (a, b) => getMax(a,b);

            //Console.WriteLine(md(4,40));

            myDel1 md1 = (int a) => a + 1;
            Console.WriteLine(md1(5));
            
			//一个参数，“（）“可以省略；
            myDel1 md2 = a => a + 1;
            Console.WriteLine(md2(5));

        }

        static int getMax(int a, int b)
        {
            return a > b ? a : b;
        }
    }
```

## 16事件

```c#
1.定义委托
    格式： 【访问修饰符】 delegate 数据类型 委托名
	例子：public delegate void myDelegate();

2. 定义事件
    格式：【访问修饰符】 even 委托名 事件名
    例子：public event myDelegate classEvent;
3.在主方法中订阅事件
    例子： t.classEvent += s1.sleep;
4.写一个引发事件的方法,调用时便代表着引发该事件
  public void Teach()
  {
      Console.WriteLine("开始上课了");
      //引发
      if(classEvent != null) //没添加订阅时为null
      {
          classEvent();  
      }
  }
5.在主方法中引发事件
  t.Teach();
```

例子：

```c#
 class Program
    {
        static void Main(string[] args)
        {
            //实例化学生
            Student s1 = new Student("薛晖");
            Student s2 = new Student("振国");
            Student s3 = new Student("志文");
            Student s4 = new Student("嘉远");

            Teacher t = new Teacher();

            //3.订阅事件
            t.classEvent += s1.sleep;
            t.classEvent += s2.playPhone;
            t.classEvent += s3.study;
            t.classEvent += s4.talk;
            //5.引发事件
            t.Teach();

        }
    }

    //广播者
    class Teacher
    {
        //1.定义委托
        public delegate void myDelegate();
        //2.定义事件(event)
        //访问修饰符 event 委托名 事件名
        //属性与字段：对字段进行封装
        //事件与委托：事件对委托进行封装
        public event myDelegate classEvent;

        //4.写一个引发事件的方法,调用时便代表着引发该事件
        public void Teach()
        {
            Console.WriteLine("开始上课了");
            //引发
           if(classEvent != null) //没添加订阅时为null
            {
                classEvent();  
            }
        }

    }

    //订阅者
    class Student
    {
        string _name;

        //对字段进行初始化：构造方法
        public Student(string name)
        {
            _name = name;
        }

        //玩手机， 睡觉，讲话，学习
        public void playPhone()
        {
            Console.WriteLine("{0}在玩手机",_name);
        }
        public void sleep()
        {
            Console.WriteLine("{0}在睡觉", _name);
        }
        public void talk()
        {
            Console.WriteLine("{0}在讲话", _name);
        }
        public void study()
        {
            Console.WriteLine("{0}在学习", _name);
        }
    }
```

## 17.1继承

语法

```c#
class 类名：父类类名
{
	
}
//子类会继承父类中的：字段，方法, 属性 ，（ 构造方法不会被继承 ）

```

实例：

```c#
 class Animal  //动物类
    {
        //访问修饰符：public ,internal, protected,( internal protected), private
        //protected string _name;
        //protected string _gender;
        //protected int _age;
        public string Name { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        //protected:定义好，外部类不能用，子类可以用，子类的子类：（应该可以）
        public void test()
        {
            Console.WriteLine("我是一头{0}的{1},",Gender,Name);
        }
    }

    //子类:父类
    //子类会继承父类中的：字段，方法,属性 ， 构造方法不会被继承
    
    class Carn : Animal
    {      
        public void test1()
        {
            Console.WriteLine("我是食肉动物");
        }
    }

    class Lion : Carn
    {
        public void test3()
        {
            Console.WriteLine("我是一头{0}的,{1}岁的{2}", Gender,Age, Name);
        }
    }

    class Herb : Animal
    {
        //public int Age
        //{
        //    get { return _age; }
        //    set { this._age = value; }
        //}
        //public string Name
        //{
        //    get { return _name; }
        //    set { this._name = value; }
        //}

        //public string Gender
        //{
        //    get { return _gender; }
        //    set { this._gender = value; }
        //}
        public void test2()
        {
            Console.WriteLine("我是一头{0}的,{2}的{1}食草动物", Gender, Name, Age);
        }
    }
```

```c#
static void Main(string[] args)
        {
            //Carn c1 = new Carn();
            //c1.Age = 18;
            //c1.Name = "狮子";
            //c1.Gender = "青春期";

            //Animal a = new Animal();
            //a.test();

            //Herb h1 = new Herb();
            //h1.Age = 18;
            //h1.Name = "奶牛";
            //h1.Gender = "老年期";
            //h1.test();
            //h1.test2();
            Console.WriteLine("--------------");
            //c1.test();
            //c1.test1();
            //Lion
            Lion sLion = new Lion();
            sLion.Name = "辛巴";
            sLion.Gender = "雄";
            sLion.Age = 5;
            sLion.test3();
        }
```

## 17.2继承中的屏蔽基类成员

例子：

```c#
class Birds
    {
        public int age=1;
        public string gender;
        protected static int wings = 2;


        public void Fly()
        {
            Console.WriteLine("鸟会飞");
        }
    }

    class Ostrich:Birds  //鸵鸟
    {
        //屏蔽基类成员，方法，属性的关键字:new
        //还可以屏蔽基类的静态成员,如果需要访问，直接用类名.字段
        new string age = "10";
        new protected static string wings="没翅膀";
        new public  void Fly()
        {
            Console.WriteLine("鸵鸟有{0}对翅膀,不会飞", Birds.wings);
            Console.WriteLine("鸵鸟{0}岁,不会飞", base.age);
        }
    }
```



## 17.3继承中的构造方法

1.使用base指定**基类**的构造函数

​			例子：

```c#
 class Person
    {
        protected string name;   //全局变量：声明+初始化
        protected int age;
        //默认存在一个 隐式的无参的构造方法
        //只要定义了显式的构造方法，那么默认的隐式无参构造方法就会消失

        //public Person()
        //{
        //    Console.WriteLine("这是Person父类中的无参构造方法");
        //}

        public Person(string name)
        {
            this.name = name;
            Console.WriteLine("这是Person父类中的带参构造方法");
        }
        public Person(string name,int age)
        {
            this.name = name;
            this.age = age;
            Console.WriteLine("这是Person父类中的带2个参数构造方法");
        }

        

    }

    class Student : Person
    {
        string _stuno;
        //默认存在一个 隐式的无参的构造方法


        //base:调用父类中指定的构造方法
        public Student(int age, string _stuno,string name):base(name,age)
        {
            this._stuno = _stuno;
            Console.WriteLine("这是Student子类中带参的构造方法");
        }

        public void Intro()
        {
            Console.WriteLine("姓名:{0}\n年龄:{1}\n学号:{2}", name, age,_stuno);
        }
    }
```



2.使用this指定**当前类**的构造函数

例子：

```c#
class Graph
    {
        double _peri;
        double _area;
        double _vol;

        public Graph(double peri,double area)
        {
            this._area = area;
            this._peri = peri;
            Console.WriteLine("2参");
        }

        //this：可以指定当前类的其它构造方法。
        public Graph(double vol,double peri, double area):this(peri,area)
        {
            this._vol = vol;
            Console.WriteLine("1参");
        }
    }
```

## 18密封类

关键字： sealed

作用：一个类不被其他类所继承

例子：

```c#
sealed class Cirele
{

}
```

## 19虚方法

关键字：virtual,override

例子

```c#
class A
{
    public virtual void VirtualMethod()   //虚方法的定义
    {
        Console.WriteLine("这是基类的虚方法")
    }
}

class B:A
{
    public override void VirtualMethod()  //虚方法的重写
    {
        Console.WriteLine("这是在子类中对基类的虚方法进行重写")
    }
}
```

## 20抽象类

定义：抽象类包含零个或者多个抽象方法，也可以包含零个或者多个非抽象方法

关键字：abstract

格式:

```c#
[访问修饰符] abstract class 类名
{
    
}
```

例子：

```c#
 abstract class AbstractDemo
    {

        public abstract int Radius { get; set; }   //抽象属性

        public abstract void GetArea(); //抽象成员：方法，属性，索引器，事件,不能抽象字段
    }

    class Circle : AbstractDemo   //子类可以是抽象类，也可以不是
    {
        int _r;

        public override int Radius { get => _r; set => _r = value; }

        public override void GetArea()
        {
            Console.WriteLine("面积:{0}",Math.PI*_r*_r);
        }
    }




class Program
    {
        static void Main(string[] args)
        {
            AbstractDemo a = new Circle() { Radius = 5};
            a.GetArea();

        }
    }
```



## 21接口

注意点：

1. 只能声明方法、属性、委托
2. 默认public
3. c#中的接口以大写的字母 I 开头
4. 接口中的所有方法、属性、索引器都必须没有实现
5. 

例子：

```c#
//interface 接口名（大写I开头）
    interface IDemo 
    {
        //接口体:方法，属性，索引器，事件
        //方法
        //返回值类型  方法名
        string GetName();   //返回string类型，获取姓名
        string GetAge();  //获取年龄

    }
    class TestA:IDemo
    {
        string _name;
        int _age;

        public TestA(string name, int age)
        {
            _name = name;
            _age = age;
        }

        public string GetName()
        {
            return _name;
        }

        public string GetAge()
        {
            return _age.ToString();
        }
        

        
    }

    class TestB:IDemo
    {
         string _surName;
         string _lastName;
         double _age;

        public TestB(string sname,string lname,double age)
        {
            _surName = sname;
            _lastName = lname;
            _age = age;
        }


        public string GetName()
        {
            return _surName+_lastName;
        }

        public string GetAge()
        {
            return _age.ToString();
        }

    }
```

```c#
class Program
    {
        static void Test(IDemo a)  //测试方法
        {
            Console.WriteLine("姓名:{0}\n年龄:{1}",a.GetName(),a.GetAge());
        }
        
        static void Main(string[] args)
        {
            IDemo a = new TestA("YIYIYI",18) ;
            Test(a);
            Console.WriteLine("----------------------------");
            IDemo b = new TestB("YI","YIYI",18.00000000);
            Test(b);
            
            
        }
    }
```

## 21.1   多重接口的实现

例子：

```c#
class Point
{
    
}

interface IPrint
{
    void Print();
}

interface IDraw
{
    void Draw();
}


class Circle:Point,IDraw,IPrint //继承父类在先，接口名在后
{
    
    
    //实现所有接口的方法
    public void Print()
    {
        
    }
    public void Draw()
    {
        
    }
}
```

## 21.2   is,as关键字

作用：检测一个实例是不是另外一种类型的

例子：

```c#
interface IFly  //1M
    {
        void Fly();
    }

    interface ISwim
    {
        void Swim();
    }

    class Fish : ISwim   //(3M)
    {
        
        public void Swim() { Console.WriteLine("游泳"); }
    }

    class Bird : IFly
    {
        public void Fly() { }
    }
```

```c#
class Program
    {
        static void Main(string[] args)
        {
            
            Fish f = new Fish();

            //去判定鱼能不能飞
            //is关键字检查是不是实现了IFly接口

            //如果没有实现，返回一个false， 如果实现了 返回一个true
            if (f is ISwim)//检查是不是实现了IFly接口
            {
                ISwim o = (ISwim)f;
                o.Swim();
            }
            else
            {
                Console.WriteLine("不会游泳");
            }


            Bird b = new Bird();

            //测试是不是会游泳
            //b as ISwim: 尝试将b对象转换为ISwim接口，
            //如果转换失败：将null赋给o1;
            //如果转换成功 ： 说面b实现了ISwim接口，再赋给 o1
            
            ISwim o1 = b as ISwim;   //ISwim o1 = new Bird()

            if(o1 != null)
            {
                o1.Swim();
            }
            else
            {
                Console.WriteLine("不会游泳");
            }
        }
    }
```

## 21.3接口绑定

定义：将不同的接口结合在一起，形成一个全新的接口

```c#
interface IDrawAndPrint : IDraw,IPrint
{
    
}
```

## 22常用类

###22.1Random类

产生一个伪随机数；

```c#
Random rabdom=new Random();//范围[0-1）
Random rabdom=new Random(10);//范围[0-10）
```

### 22.2DateTime结构

```c#
     		Console.WriteLine(DateTime.Now); //本地时间
            Console.WriteLine(DateTime.UtcNow); //本初子午线时间
            Console.WriteLine(DateTime.Today);

            DateTime d = new DateTime(2022, 5, 2,6,6,6);
            //转换成只有年月日的日期
            Console.WriteLine(d.ToString("yyyy/MM/dd"));
            Console.WriteLine(d.ToShortDateString());
```

### 22.3string类

```c#
//特征：不可变性
string.Join("",)//用指定分隔符连接多个字符串；
```

### 22.4 StringBuiLder类

```c#
 			StringBuilder://具有可变性
            StringBuilder s1 = new StringBuilder("0123456789");
            s1.Append("b");  //添加
            Console.WriteLine(s1);
            s1.Replace("a", "A");  //替换
            Console.WriteLine(s1);
            s1.Remove(4,4);   //移除, （startIndex, length）:从下标几开始，删除几个字符
            Console.WriteLine(s1);
```

### 22.5正则表达式

```

```

## 23ArrayList类（集合）

```c#
命名空间：
using.System.Collections;
定义：
ArrayList list=new ArrayList
实例：
//Array集合:有序的，元素类型不一定，不定长
            //实例化
            Student s1 = new Student("志文", "001");
            Student s2 = new Student("李涛", "002");
            Student s3 = new Student("周飘", "007");

            Teacher t = new Teacher("蜗牛", "005");
            //1.定义集合：空的
            ArrayList list = new ArrayList();
            //2.添加元素
            list.Add(s1);   //在下标为0的地方存入了元素s1
            list.Add(s2);
            list.Add(s3);
            list.Add(t);    //添加一个老师对象t

            //查看list里的元素个数
            Console.WriteLine("list元素个数"+ list.Count);
            //查看第i个元素
            Console.WriteLine("第三个元素"+((Student)list[2]).name);

            //3.遍历元素
            Traverse(list);

            //4.删除元素
            //4.1 指定元素删除
            //list.Remove(t);
            //4.2 指定下标删除
            //list.RemoveAt(3);

            //删除之后
            Console.WriteLine("-----------------");
            Console.WriteLine("删除后:");
            Traverse(list);

            //5.插入元素
            //跟Add区分： Add：添加在元素的最后面
            //Insert：往指定位置进行插入
            Student s4 = new Student("金星", "003");
            list.Insert(2,s4);
            //插入后遍历
            Console.WriteLine("插入后：");
            Traverse(list);

            //  清空集合
            list.Clear();
            Console.WriteLine("清楚后的集合长度"+list.Count);
```

##24Hashtable类

```c#
命名空间：
using.System.Collections;
实例：
using System;
using System.Collections;

namespace Hashtable集合
{
    class Program
    {
        static void Main(string[] args)
        {
            //ArrayList：有序的，动态长度，元素类型可以不一致
            //Hashtable：无序的，动态长度，元素类型可以不一致
            Student s1 = new Student("志文", "001");
            Student s2 = new Student("李涛", "002");
            Student s3 = new Student("周飘", "007");

            Teacher t = new Teacher("蜗牛", "005");
            //1.定义HashTable集合
            Hashtable hlist = new Hashtable();

            //2.添加元素
            hlist.Add(s1.stuNo, s1);
            hlist.Add(s2.stuNo, s2);
            hlist.Add(s3.stuNo, s3);
            hlist.Add(t.teaNo, t);


            //3.查找元素
            Console.WriteLine("查找学号为007的学生" + ((Student)(hlist["007"])).name);
            //4删除元素
            hlist.Remove("005");
            foreach (string i in hlist.Keys)
            {
                Console.WriteLine(i);
            }
            //5清空
            hlist.Clear();
            Console.WriteLine(hlist.Count);
            
			//4.1遍历键: string   集合.Keys
            foreach(string i  in hlist.Keys)
            {
                Console.WriteLine(i);
            }
            //4.2遍历值 集合.Values
            foreach(var i in hlist.Values)
            {
                if(i is Student)
                {
                    Student obj = (Student)i;
                    Console.WriteLine("学号{0},姓名{1}",obj.stuNo,obj.name);
                }
                else
                {
                    Teacher obj = (Teacher)i;
                    Console.WriteLine("教师号{0},姓名{1}", obj.teaNo, obj.name);
                }
            }
        }
    }
}

```

## 25泛型集合

### 25.1

```c#
			//List<T>:T：代表集合里面的数据类型
            List<int> list = new List<int>() {11,12,13 };
            //AddRange:添加一个集合
            list.AddRange(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Console.WriteLine(string.Join(" ", list));

            //insert insertRange()
            //list.Insert(3,0)
            //list.InsertRange(3, new int[] { 0, 0, 0 });
            //Console.WriteLine(string.Join(" ", list));

            //clear()：清除数据，内存还在

            //Console.WriteLine($"count:{ list.Count},capacity{list.Capacity}");
            //list.Clear();
            //Console.WriteLine($"count:{ list.Count},capacity{list.Capacity}");

            //Remove() RemoveAt() RemoveRange  RemoveAll()
            //list.Remove(0);根据元素清除
            //list.RemoveAt(3);根据下表清除
            //list.RemoveRange(3, 3);第3个元素往后清除三个
            //list.RemoveAll(x => x % 2 == 0);根据条件清除全部
            //Console.WriteLine(string.Join(" ", list));
			//  list.Contains(1);判断括号里的值是否属于list,是则返回true，否则返回false;
            List<int> listA = list.GetRange(0, 5);
            for (int i = 0; i < listA.Count; i++)
            {
                listA[i] += 10;
            }
            Console.WriteLine(string.Join(" ",listA));
            Console.WriteLine("原来的：");
            Console.WriteLine(string.Join(" ",list));

```







